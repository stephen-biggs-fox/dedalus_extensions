# dedalus_extensions

This python package provides extensions to the PDE solver framework
[Dedalus](http://dedalus-project.org/). Amongst other things, this
includes the following functionality:

- Dedalus models can be run using the command

        $ dedalus run MODEL_NAME

- Models are defined by extending a generic `PhysicsModel` class and
  implementing the abstract methods `setUp` and `solve`. The base
class reads physical and numerical parameters from a configuration
file, thus allowing a model to be written once and re-used with
different configuration files rather than maintaining many copies of
the model with minor differences between them.

- TODO: Dedalus post processing functions are provided and are
  accessible via the command

        $ dedalus process PROCESS_NAME

- The Dedalus environment is activated automatically when running the
  commands above.

# Installation

To install, first set the environment variable:

    $ export DEDALUS_HOME=/path/to/dedalus/installation

This is required to both install and run dedalus_extensions so add it
to your ~/.bashrc.

Then clone the repository and run the install script:

    $ git clone https://gitlab.com/steve_biggs/dedalus_extensions.git
    $ cd dedalus_extensions
    $ ./INSTALL.sh

The install script simply copies the dedalus_extensions source files
to the appropriate place within your dedalus installation. The
directory path is printed so that you can add it to your `$PATH`
environment variable.

# Updates

To obtain updates, simply pull from the remote git repository and run
the install script again:

    $ cd dedalus_extensions
    $ git pull
    $ ./INSTALL.sh
