"""
Unit tests of PhysicsModel

Copyright 2018 Stephen Biggs-Fox

This file is part of dedalusextensions.

dedalusextensions is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dedalusextensions is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dedalusextensions.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase, mock
import sys
sys.path.insert(0, '../src')
from PhysicsModel import PhysicsModel


class PhysicsModelTest(TestCase):

    def setUp(self):
        self.model = PhysicsModel()
        self.mockConfigParser = mock.Mock()
        self.runConfigFilePath = 'path/to/run.cfg'

    def testPhysicsModelReadsConfigFile(self):
        self.model._readConfig(self.mockConfigParser, self.runConfigFilePath)
        self.mockConfigParser.read.assert_called_once_with(self.runConfigFilePath)

    def testPhysicsModelHoldsOntoConfig(self):
        self.model._readConfig(self.mockConfigParser, self.runConfigFilePath)
        self.model.config.read.assert_called_once_with(self.runConfigFilePath)

    def testSetUpRaisesNotImplmentedError(self):
        with self.assertRaises(NotImplementedError):
            self.model.setUp()

    def testSolveRaisesNotImplementedError(self):
        with self.assertRaises(NotImplementedError):
            self.model.solve()

