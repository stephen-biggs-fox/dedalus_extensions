"""
Unit tests of Main

Copyright 2018 Stephen Biggs-Fox

This file is part of dedalusextensions.

dedalusextensions is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dedalusextensions is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dedalusextensions.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase, mock
import sys
sys.path.insert(0, '../src')
from Main import Main


class MainTest(TestCase):

    def setUp(self):
        self.de = Main()

    def testMainCallsCreateParser(self):
        mockCreateParserMethod = mock.Mock()
        self.de.createParser = mockCreateParserMethod
        self.de.main()
        self.assertTrue(mockCreateParserMethod.called)

    def testMainRunsDedalusWithGivenModelAndDefaultsWhenFirstArgIsRun(self):
        mockNewRunner = mock.Mock()
        self.de.newRunner = mockNewRunner
        modelName = 'hw'
        self.de.main(*['run', modelName])
        mockNewRunner.assert_called_once_with(modelName, self.de.defaultModelDir, self.de.defaultRunDir)

    def testMainRunsDedalusWithGivenModelAndDirsWhenDirOptionsUsed(self):
        mockNewRunner = mock.Mock()
        self.de.newRunner = mockNewRunner
        modelName = 'hw'
        modelDir = '.'
        runDir = '.'
        self.de.main(*['run', modelName, '-m', modelDir, '-r', runDir])
        mockNewRunner.assert_called_once_with(modelName, modelDir, runDir)

    def testMainProcessesDedalusDataWhenFirstArgIsProcess(self):
        mockNewProcessor = mock.Mock()
        self.de.newProcessor = mockNewProcessor
        process = 'check'
        self.de.main(*['process', process])
        mockNewProcessor.assert_called_once_with(process)
