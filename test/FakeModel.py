"""
Fake model file for dynamic import and run

Copyright 2018 Stephen Biggs-Fox

This file is part of dedalusextensions.

dedalusextensions is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dedalusextensions is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dedalusextensions.  If not, see http://www.gnu.org/licenses/.
"""


class FakeModel:

    def __init__(self):
        self.wasInstantiated = True
        self.wasConfigRead = False
        self.wasSetUp = False
        self.wasSolved = False
        self.runConfigFilePath = None

    def _readConfig(self, runConfigFilePath):
        self.wasConfigRead = True
        self.runConfigFilePath = runConfigFilePath

    def setUp(self):
        self.wasSetUp = True

    def solve(self):
        self.wasSolved = True
