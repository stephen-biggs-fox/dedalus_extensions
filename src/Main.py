"""
Main program to run Dedalus from the command line with user options

Copyright 2018 Stephen Biggs-Fox

This file is part of dedalusextensions.

dedalusextensions is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dedalusextensions is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dedalusextensions.  If not, see http://www.gnu.org/licenses/.
"""

import sys
import argparse
from Runner import Runner
from Processor import Processor


class Main:

    def __init__(self):
        self.defaultModelDir = '../model'
        self.defaultRunDir = './run.cfg'

    def main(self, *args):
        parser = self.createParser()
        parsedArgs = parser.parse_args(args)
        if parsedArgs.command == ['run']:
            self.newRunner(parsedArgs.arg[0], parsedArgs.model_dir, parsedArgs.run_config_file)
        elif parsedArgs.command == ['process']:
            self.newProcessor(parsedArgs.arg[0])

    def createParser(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('command', nargs=1, help="The action to take ('run' or 'process')")
        parser.add_argument('arg', nargs=1, help="The argument to the command (MODEL_NAME if 'run'; PROCESS_NAME if 'process')")
        parser.add_argument('-m', '--model_dir', help="The directory in which to find the model (use with 'run' command; default '{}')".format(self.defaultModelDir), default=self.defaultModelDir)
        parser.add_argument('-r', '--run_config_file', help="The relative path to the run configuration file (use with 'run' command; default '{}')".format(self.defaultRunDir), default=self.defaultRunDir)
        return parser

    def newRunner(self, modelName, modelDir, runConfigFile):
        return Runner(modelName, modelDir, runConfigFile)

    def newProcessor(self, process):
        return Processor(process)


# Untested (untestable?) code to run main when executed as a script
if __name__ == '__main__':
    main = Main()
    main.main(*sys.argv[1:])
