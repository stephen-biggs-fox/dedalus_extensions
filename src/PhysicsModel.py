"""
Base class representing a physics model to be extended by the user

Copyright 2018 Stephen Biggs-Fox

This file is part of dedalusextensions.

dedalusextensions is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dedalusextensions is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dedalusextensions.  If not, see http://www.gnu.org/licenses/.
"""


class PhysicsModel:

    def _readConfig(self, configParser, runConfigFilePath):
        self.config = configParser
        self.config.read(runConfigFilePath)

    def setUp(self):
        raise NotImplementedError('setUp() must be overridden in subclasses of PhysicsModel')

    def solve(self):
        raise NotImplementedError('solve() must be overridden in subclasses of PhysicsModel')
