"""
Class responsible for running a Main simulation

Copyright 2018 Stephen Biggs-Fox

This file is part of dedalusextensions.

dedalusextensions is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dedalusextensions is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dedalusextensions.  If not, see http://www.gnu.org/licenses/.
"""
import sys


class Runner:

    def __init__(self, modelName, modelDir, runConfigFilePath):
        self.modelName = modelName
        self.modelDir = modelDir
        self.runConfigFilePath = runConfigFilePath

    def run(self):
        self.model = self._loadModel()
        self.model._readConfig(self.runConfigFilePath)
        self.model.setUp()
        self.model.solve()

    def _loadModel(self):
        sys.path.insert(0, self.modelDir)
        modelModule = __import__(self.modelName, fromlist=[self.modelName])
        Model = getattr(modelModule, self.modelName)
        return Model()
