#! /bin/bash
# Script to install dedalus_extensions
# Optionally takes one argument which is passed on to rsync

# Check ${DEDALUS_HOME} is defined before beginning
if [ -z ${DEDALUS_HOME} ]
then
    >&2 echo 'ERROR: Environment variable ${DEDALUS_HOME} not defined. Please set up before running this script'
    exit 1
fi

# Print start message
program_name="dedalus_extensions"
echo ""
echo "Installing ${program_name}..."
echo ""
echo ""


# Copy source files to installation directory
dex_dir=lib/python3.6/site-packages/${program_name}
install_dir=${DEDALUS_HOME}/${dex_dir}
echo "Copying source files to ${install_dir}..."
echo ""
rsync -cvr --delete --exclude=__pycache__ src/ ${install_dir}/ $1
echo ""
echo ""

# Setup symlink so running processes are called dedalus not python3
# TODO - should probably check that dedalus isn't already an alias or program
dedalus_bin=${DEDALUS_HOME}/bin
symlink_target=${dedalus_bin}/python3
symlink_name=${dedalus_bin}/dedalus
echo "Checking symlink ${symlink_name} (target ${symlink_target})..."
echo ""
ln -s ${symlink_target} ${symlink_name} 2>/dev/null  # Supress error output
if [ $? != 0 ]
then
    if [ -L ${DEDALUS_HOME}/bin/dedalus ]
    then
        if [ ! "$(readlink ${symlink_name})" = "${symlink_target}" ]
        then
            echo "symlink ${symlink_name} exists but does not point to ${symlink_target}. Please fix this manually"
        else
            echo "symlink already exists and points to correct target"
        fi
    else
        echo "file ${symlink_name} exists but is not a symlink. It should be and should point to ${symlink_target}. Please fix this manually."
    fi
else
    echo "symlink created"
fi
echo ""
echo ""

# Add alias for running the program (must be an alias as the shell script has to be sourced not run)
# TODO - should probably check that dex isn't already an alias or program
dex_command="dex"
alias_text='alias '${dex_command}'="source '${install_dir}'/dex.sh"'
alias_file=${HOME}/.bash_aliases
alias_name="alias '${dex_command}'"
echo "Checking ${alias_text}..."
echo ""
# Source aliases file before checking and possibly adding the alias so that it is active within this
# subshell if it was active within the parent shell
shopt -s expand_aliases
source ${alias_file}
grep "${alias_text}" ${alias_file} > /dev/null
if [ $? != 0 ]
then
    echo "Adding ${alias_text} to ${alias_file}..."
    echo ""
    echo "${alias_text}" >> ${alias_file}
else
    echo "${alias_name} already exists in ${alias_file}"
    echo ""
fi
type ${dex_command} > /dev/null 2>&1
if [ $? != 0 ]
then
    echo "${alias_name} will be active next time ${alias_file} is sourced"
    echo ""
    echo "You can activate this now using:"
    echo ""
    echo "    $ source ${alias_file}"
else
    echo "${alias_name} is already active"
fi
echo ""
echo ""

# Report successful completion
echo "${program_name} successfully installed. Enter '${dex_command} -h' for help getting started"
echo ""
